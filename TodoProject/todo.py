from datetime import *


class Todo(object):

    def __init__(self, message, id, done, created=datetime.now()):
        self.message = message
        self.id = id
        self.done = done
        self.created = str(created)

    def __str__(self):
        return ' Id [%s]-> Message : %s - [%s] - Created :%s' % (self.id , self.message, self.done, self.created)

    def isDone(self):
        self.done = True

