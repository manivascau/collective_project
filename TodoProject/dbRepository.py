import db_handler
import todo


class DBRepository(object):

    def __init__(self, fileName):
        self.fileName = fileName
        self._todo_list = []
        self._counter = 1
        self.load_tasks(fileName)

    def insert_task(self, fileName):
        """ Insert last element from _todo_list in resource """
        new_task = self._todo_list[len(self._todo_list) - 1]
        query = "insert into TASK(message,done,id,created) values (?,?,?,?)"
        new_task.done = 0
        db_handler.write_to_resource(fileName, query, new_task.__dict__.values())

    def load_tasks(self, fileName):
        """ Loads existing tasks to _todo_list """
        query = "select message,done,id,created from TASK"
        rf = db_handler.read_from_resource(fileName, query)
        for line in rf:
            to_do_obj = todo.Todo(line['message'], line['id'], bool(line['done']), line['created'])
            self._todo_list.append(to_do_obj)
            self._counter = self._counter + 1

    def add_todo(self, message):
        """ Adds a todo object to _todo_list """
        task = todo.Todo(message, self._counter, False)
        self._todo_list.append(task)
        self.insert_task(self.fileName)

    def get_todo_list(self):
        """ Obtain list of not done tasks """
        todo_list = [task for task in self._todo_list if not task.done]
        return todo_list

    def mark_task_as_done(self, id):
        """  Marks a task as completed based on id """
        query = "update TASK set done = 1 WHERE id =(?)"
        db_handler.update_resource(self.fileName, query, id)
