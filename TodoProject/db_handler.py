import sqlite3


def read_from_resource(fileName, query):
    result_list = []
    with sqlite3.connect(fileName) as connection:
        with connection as cursor:
            cursor = connection.cursor()
            cursor.execute(query)
            rows = cursor.fetchall()
            for row in rows:
                result_list.append({'message': row[0],
                                    'done': row[1],
                                    'id': row[2],
                                    'created': row[3]
                                    })
            return result_list


def write_to_resource(fileName, query, task):
    with sqlite3.connect(fileName) as connection:
        with connection as cursor:
            cursor.execute(query, task)
            connection.commit()

def update_resource(fileName,query,id):
    with sqlite3.connect(fileName) as connection:
        with connection as cursor:
            cursor.execute(query,(id,))
            connection.commit()
