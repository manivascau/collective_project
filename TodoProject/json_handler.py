import json
import os

def write_to_json(entry,fileName):
    a = []
    if not os.path.isfile(fileName):
        a.append(entry)
        with open(fileName, mode='w') as f:
            f.write(json.dumps(entry, indent=2))
    else:
        with open(fileName) as last_version:
            last_version_content = json.load(last_version)

        last_version_content.append(entry)
        with open(fileName, mode='w') as f:
            f.write(json.dumps(last_version_content, indent=2))

def read_from_json(fileName):

    with open(fileName) as data_file:
        data = json.load(data_file)
    return data


def update_json(fileName,key,value,id):

    with open(fileName, 'r') as f:
        json_data = json.load(f)
        for el in json_data :
            if id==el['id']:
                el['done']=value
        # element_to_update = [el for el in json_data if ]
        # json_data[0][key] = value

    with open(fileName, 'w') as f:
        f.write(json.dumps(json_data))
