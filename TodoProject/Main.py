import dbRepository
import jsonRepository
import sys
import argparse
from datetime import *
import todo_settings
import xml.etree.cElementTree as elem


if todo_settings.STORAGE_TYPE == "sqlite":
    r = dbRepository.DBRepository(todo_settings.STORAGE_PATH)
else:
    r = jsonRepository.JSONRepository(todo_settings.STORAGE_PATH)


def convert_time(time):
    """Convert time elapsed from current time"""
    time_results = datetime.now() - datetime.strptime(time, "%Y-%m-%d %H:%M:%S.%f")
    if time_results.days != 0 :
        return str(time_results).split(',')[0] + " ago"
    #Check if hours
    elif str(time_results).split(':')[0] != '0' :
        return str(time_results).split(':')[0] + " hour{ending} ago".\
            format(ending="s" if int(str(time_results).split(':')[0]) > 1 else "")
    #Check if minutes
    elif str(time_results).split(':')[1] != '00' :
        return str(int(str(time_results).split(':')[1])) + " minute{ending} ago".\
            format(ending="s" if int(str(int(str(time_results).split(':')[1]))) > 1 else "")
    #Check if seconds
    elif str(time_results).split(':')[2] != '00' :
        return str(int(str(time_results).split(':')[2].split(".")[0])) + " second{ending} ago".\
            format(ending="s" if int(str(int(str(time_results).split(':')[2].split(".")[0]))) > 1 else "")


def convert_tasks_elapsed_time(tasks):
    """Converts time of tasks to elapsed time"""
    for task in tasks :
        task.created = convert_time(task.created)


def write_xml_to_file(list_of_tasks,file_name="task.xml"):
    root = elem.Element("Tasks")
    for task in list_of_tasks:
        task_root = elem.SubElement(root, "Task")

        elem.SubElement(task_root, "id", name='id').text=str(task.id)
        elem.SubElement(task_root, "message", name='message').text = task.message
        elem.SubElement(task_root, "done", name='done').text = task.done
        elem.SubElement(task_root, "created", name='created').text = task.created
        tree = elem.ElementTree(root)
    tree.write(open(file_name, 'w'))



def print_todo_list(args) :
    """Prints elements of todo list"""
    todo_list = r.get_todo_list()
    if args.format is not None and args.format[0]=='xml':
        write_xml_to_file(todo_list)
    convert_tasks_elapsed_time(todo_list)
    #For each task in list
    for task in todo_list :
        print str(task) + "\n"



def add_todo(args):
    """Adds a task"""
    message = ' '.join(args.message)
    r.add_todo(message)


def mark_as_done(args) :
    """Marks a task as done"""
    for id in args.id :
        r.mark_task_as_done(id)


parser = argparse.ArgumentParser()

subparsers = parser.add_subparsers(help="Action to be made on the to-do list.")
#List command subparser
parser_list = subparsers.add_parser('list',help="Lists in progress tasks.")
parser_list.add_argument('--format',help="Format of the output file",nargs='*' )
parser_list.set_defaults(func=print_todo_list)

#Add command subparser
parser_add = subparsers.add_parser('add',help = "Adds a new task.")
parser_add.add_argument('message',nargs="+",help="Specify the message.")
parser_add.set_defaults(func=add_todo)

#Done command subparser
parser_done = subparsers.add_parser('done',help = "Marks a task as done.")
parser_done.add_argument('id',nargs='*',type=int,help="Specify id of the task")
parser_done.set_defaults(func=mark_as_done)

#parse the args
args = parser.parse_args()
args.func(args)




