# Exercises for notions learned in unit 1
# ---------------------------------------

# takes a message, capitalizes the first letter then underlines the message with =
# print make_title('ana')
# Ana
# ===
# use the string method upper
def make_title(message):
    return message.title()

# given a list of tuples of length 2 it returns a list of the tuples inverted
# if both values in the tuple are the same then we omit them from the output
# [(2, 4), (3, 3), (1, 2)] will become [(4, 2), (2, 1)]

def invert_tuples(lst):
    ret = []
    ret =[(tup[1],tup[0]) for tup in lst if tup[0] != tup[1]]
    return ret

# curses is a dictionary of cursewords to censored curses
# Given a list of words replace the curses according to the dict
# censor(['you', 'fucker'], {'fucker': 'f**er'}) should return
# ['you', 'f**er']
def censor(lst, curses):
    ret = []
    for word in lst :
        try :
            ret.append(curses[word])
        except KeyError :
            ret.append(word)
    return ret

# Given a string s, return a string made of the first 2
# and the last 2 chars of the original string,
# so 'spring' yields 'spng'. However, if the string length
# is less than 2, return instead the empty string.
def both_ends(s):
    if len(s)< 2 :
        return ''
    return s[0]+s[1]+s[-2]+s[-1]

# return the factorial of n
# factorial(5) = 1*2*3*4*5
def factorial(n):
    if not n :
        return 1
    fact = 1
    for number in xrange(2,n+1) :
        fact *= number
    return fact

def invert_dict(d):
    """
    >>> invert_dict({'ana': 4, 'are': 5})
    {4: 'ana', 5: 'are'}
    """
    dic={}
    for key in d :
        dic[d[key]]=key
    return dic


def has_no_repeating_chars(string):
    """
    in the word "eleven" e appears 3 times.
    return True if string contains no repeated letters
     has_no_repeating_chars('eleven')
    False
     has_no_repeating_chars('')
    True
     has_no_repeating_chars('python')
    True
    """
    if not string :
        return True
    set_of_chars = set(string)
    if len(set_of_chars)==len(string) :
        return True
    else :
        return False


def flatten_list(lst):
    """
    lst is a list. It may contain other lists.
    Return a flattened version of lst
     flatten_list([1, 2, [3, 4], [5, [6, 7], 8]])
    [1, 2, 3, 4, 5, 6, 7, 8]
     flatten_list([ [44, 11], [99, 33] ])
    [44, 11, 99, 33]
    """
    ret = []
    for element in lst :
        if isinstance(element,list) :
            ret +=flatten_list(element)
        else :
            ret.append(element)
    return ret


def splits(word):
    """
    returns a list of all posible splits of word
    include the empty splits
    returns a list of tuples.
     splits('ana')
    [('', 'ana'), ('a', 'na'), ('an', 'a'), ('ana', '')]
    """
    ret = []
    for pos in xrange(len(word)) :
        ret.append((word[0:pos],word[pos:len(word)]))
    ret.append((word,''))
    return ret
import copy
def inserts(word):
    """
    returns all misspellings of word caused by inserting a letter
    use the splits function
     'bana' in inserts('ana')
    True
     'naa' not in inserts('ana')
    True
    """
    letters = 'abcdefghijklmnopqrstuvwxyz'

    ret = []
    lst = list(word)
    for letter in letters :
        ano = copy.deepcopy(lst)
        for pos in xrange(0,len(word)+1) :
            another_lst = copy.deepcopy(ano)
            another_lst.insert(pos,letter)
            misspell = ''
            for let in another_lst :
                misspell +=let
            ret.append(misspell)
    return ret


def deletes(word) :
    """
        all misspellings of word caused by omitting a letter
        use the splits function
        >>> 'rockets' not in deletes('rocket')
        True
        >>> 'roket' in deletes('rocket')
        True
    """
    ret_lst =[]
    string=[elem for elem in word]
    for i in xrange(0,len(string)) :
        result_str=[]
        for j in xrange(0,len(string)) :
            if i != j :
                result_str += string[j]
        ret_lst.append(''.join(result_str))
    return ret_lst

def insertsWithSplits(word) :
    tuples=splits(word)
    alphabet = 'abcdefghijklmnopqrstuvwxyz'
    ret_lst = []
    for letter in alphabet :
        for t1,t2 in tuples :
            ret_lst.append(t1+letter+t2)
    return ret_lst


def deletesWithSplits(word) :
    ret_lst = []
    for tup in splits(word) :
        if tup[0] :
            ret_lst.append(tup[0]+tup[1][1:])

    return ret_lst


def test_insertsWithSplits() :
    assert insertsWithSplits('ana')==inserts('ana')









#----- testing code below ---


def test_deletes() :
    assert 'rockets' not in deletes('rocket')
    assert 'roket' in deletes('rocket')

def test_make_title():
    assert make_title('ana') == 'Ana\n==='

def test_invert_tuples():
    assert invert_tuples([(2, 4), (3, 3), (1, 2)]) == [(4, 2), (2, 1)]

def test_censor():
    assert censor(['you', 'fucker'], {'fucker': 'f**er'}) == ['you', 'f**er']

def test_both_ends():
    assert both_ends('spring') == 'spng'
    assert both_ends('Hello') == 'Helo'
    assert both_ends('a') == ''
    assert both_ends('xyz') == 'xyyz'

def test_factorial():
    assert factorial(5) == 1*2*3*4*5
    assert factorial(1) == 1
    assert factorial(0) == 1

def test_has_no_repeating_chars() :
    assert has_no_repeating_chars('eleven') == False
    assert has_no_repeating_chars('python') == True
    assert has_no_repeating_chars('') == True

def test_flatten_list() :
    assert flatten_list([1, 2, [3, 4], [5, [6, 7], 8]]) == [1,2,3,4,5,6,7,8]
    assert flatten_list([[44,11],[33,22]])==[44,11,33,22]

def test_splits() :
    assert splits('ana') == [('', 'ana'), ('a', 'na'), ('an', 'a'), ('ana', '')]

def test_inserts() :
    assert 'bana' in inserts('ana')
    assert 'naa' not in inserts('ana')