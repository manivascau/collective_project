
def toesc(c):
    """
    Returns the ansii escape with the code c
    \x1b[cm
    """
    return '\x1b[' +str(c)+ 'm'

# define the reset code
ENDC = toesc(0)
LBLACK = toesc(90)
LRED = toesc(91)
LGREEN = toesc(92)
LYELLOW = toesc(93)
LBLUE = toesc(94)
LPURPLE = toesc(95)
LCYAN = toesc(96)
LWHITE = toesc(97)
def colored_text(text, color=ENDC):
	return color+text+color


def welcome_python():
    print colored_text("Python ",LRED) + \
          colored_text("welcomes ",LGREEN) + \
          colored_text("the ",LYELLOW) + \
          colored_text("terminal.",LPURPLE)
    print ("test")

welcome_python()


