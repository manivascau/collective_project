import sqlite3
import csv


#TODO abstracization,method descriptions
def get_all_artists():
    list_of_artists = []
    conn = sqlite3.connect('chinook.db')
    cursor = conn.execute("SELECT * from ARTISTS")
    for row in cursor :
        list_of_artists.append(row)
    return list_of_artists


def get_artists_paginated(list_of_artists,page,page_size):
    """   """
    list_of_artists_pag = []
    entries_for_page = len(list_of_artists)/page_size
    starting_point = entries_for_page * page
    end_point = starting_point + page_size
    for i in xrange(starting_point,end_point):
        list_of_artists_pag.append(list_of_artists[i])
    return list_of_artists_pag


def print_formatted_artists(list_of_artists):
    """  """
    max_characters_artist = [artist[1].encode("utf-8") for artist in list_of_artists]
    m = len(max(max_characters_artist, key=len))
    for element in list_of_artists :
         white_spaces =   m - len(element[1])
         print "+" + "-"*m + "+" + "\n" + "|" + element[1] + " "* white_spaces + "|"
    print "+" + "-"*m + "+"

def get_long_tracks():
    list_of_tracks = []
    query = "select artists.Name,tracks.Name,albums.Title,tracks.Milliseconds from tracks inner join albums on tracks.AlbumId=albums.AlbumId inner join artists on albums.ArtistId = artists.ArtistId"
    conn = sqlite3.connect('chinook.db')
    cursor = conn.execute(query)
    for row in cursor:
        list_of_tracks.append(row)
    return list_of_tracks


def print_long_tracks(list_of_tracks):
    for column in list_of_tracks:
        print str(column[0]) + "   " +  str(column[1])+  "    " +str(column[2])+ "    " +str(column[3])


e = get_long_tracks()
print_long_tracks(e)
# t = get_all_artists()
# ap = get_artists_paginated(t,4,13)
#print_formatted_artists(ap)