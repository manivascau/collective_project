import urllib2
import json


def parse_page_json(url,word):
    """ Parse a xml page and return list of all titles that contain a specific word """
    list_of_titles = []
    response = urllib2.urlopen(url)
    json_loader = json.load(response)
    json_data = json_loader["data"]["children"]
    for child in json_data :
        if word in child["data"]["title"].lower():
            list_of_titles.append(child["data"]["title"])

    return list_of_titles

def print_titles(list_of_titles):
    """ Print each element of a list of titles"""
    for titles in list_of_titles:
        print titles + "\n"


if __name__ == "__main__":
    titles = parse_page_json('http://www.reddit.com/r/pythoncoding/.json','python')
    print_titles(titles)