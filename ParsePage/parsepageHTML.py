from bs4 import BeautifulSoup
import urllib2


def parse_page_html(url,word):
    """ Parse a html page and return list of all titles that contain a specific word """
    list_of_titles = []
    response = urllib2.urlopen(url)
    html = response.read()
    soup = BeautifulSoup(html, 'html.parser')

    for link in soup.find_all('a') :
        if link.get('class') is None :
            continue
        else :
            if "title" in link.get('class') and "outbound" in link.get('class'):
                if word in link.getText().lower() :
                    list_of_titles.append(link.getText())
    return list_of_titles


def print_titles(list_of_titles):
    """ Print each element of a list of titles"""
    for titles in list_of_titles:
        print titles + "\n"


if __name__ == "__main__":
    titles = parse_page_html('https://www.reddit.com/r/pythoncoding/','python')
    print_titles(titles)
