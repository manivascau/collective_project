import xml.etree.ElementTree as ET
import urllib2

def parse_page_xml(url,word):
    """ Parse a xml page and return list of all titles that contain a specific word"""
    list_of_titles = []
    response = urllib2.urlopen(url)
    xml_response = ET.parse(response)
    xml_response_root = xml_response.getroot()
    for child in xml_response_root:
        for child_of_child in child :
            #If searched word is in title
            if 'title' in child_of_child.tag and word in child_of_child.text.lower():
                list_of_titles.append(child_of_child.text)
    return list_of_titles


def print_titles(list_of_titles):
    """ Print each element of a list of titles"""
    for titles in list_of_titles:
        print titles + "\n"


if __name__ == "__main__":
    titles = parse_page_xml('https://www.reddit.com/r/pythoncoding/.xml','python')
    print_titles(titles)
