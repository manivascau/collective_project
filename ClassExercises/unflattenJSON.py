
def un_flatten(paths):
    result = dict()

    for line in paths:
        cur_dict = result
        for field in line.strip("/").split("/"):
            cur_dict = cur_dict.setdefault(field, {})
    return result

print un_flatten(['A/B/T', 'A/U', 'A/U/Z'])



def test_un_flatten():
    tree = un_flatten(['A/B/T', 'A/U', 'A/U/Z'])
    assert tree == {
        'A': {
            'B': {'T': {}},
            'U': {
                'Z': {}
            }
        }
    }
    assert tree['A']['B']['T'] == {}

