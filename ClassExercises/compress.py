
def rle(l):
    """ RLE compressing algorithm """
    new_l = []
    c = 1
    for i in range(len(l)-1):
        if l[i] == l[i+1]  :
            c +=  1
            continue
        else:
            new_l.append((l[i], c))
            c = 1
    new_l.append((l[i], c))
    return new_l


print rle([0,0,0,1,1,7,0,0,0,1])

def test_rle():
    assert rle([0,0,0,1,1,7,0,0,0,1]) == [(0, 3), (1, 2), (7, 1), (0, 3), (0, 1)]

