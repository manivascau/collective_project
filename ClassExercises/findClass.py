import os
import re


def iterate(patten_to_search,path):
    for root, dirs, files in os.walk(path):
        for name in files:
            if name[-2:] == 'py':
                file = open(name)
                for line in file:
                    if patten_to_search in  line :
                        print os.path.join(root, name)


def iterate_with_regex(path):
    for root, dirs, files in os.walk(path):
        for name in files:
            if re.search("^.*py$", name) :
                file = open(name)
                for line in file:
                    if re.search('class',line)  :
                        print os.path.join(root, name)

iterate('class','.')
print "----------------"
iterate_with_regex('.')
