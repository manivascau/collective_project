from bs4 import BeautifulSoup
import urllib2


def get_urls(url,word,number_of_articles):
    "Get urls from main page of a site which contain a specific word"
    response = urllib2.urlopen(url,'html.parser')
    html = response.read()
    soup = BeautifulSoup(html,'html.parser')
    counter = 0
    url_list = []
    #For each link in the page
    for link in soup.find_all("a") :
        #If desired word in link and it is a article to link
        if word in str(link) and 'title' in str(link) :
            counter += 1
            if counter <= number_of_articles:
                url_list.append(link)
            else :
                break
    return url_list


def get_content_of_urls(url,word,number_of_articles):
    """Returns content of a link for urls that contain a specific word"""
    #Get the list of urls to be accessed
    url_list = get_urls(url,word,number_of_articles)
    list_of_content = []
    for url in url_list:
        #Open the link
        html = urllib2.urlopen(url.attrs['href'], 'html.parser')
        soup = BeautifulSoup(html,'html.parser')
        #For each div tag
        for el in soup.find_all('div'):
            if el.get('id') == "articleContent":
                list_of_content.append(el.getText())
    return list_of_content


def print_content(content):
    """ Prints the content of article """
    for article in content :
        print article + "\n"

if __name__ == "__main__":
    content =  get_content_of_urls('http://hotnews.ro/','dna',2)
    print_content(content)
