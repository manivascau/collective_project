def gcd(a,b):
	while b != 0 :
		t = b
		t = a % b
		a = t
	return a



def test_gcd():
	assert gcd(3,12) == 3
	assert gcd(12,6) == 2
	assert gcd(13,1) == 1
test_gcd()