import os
import sys

def print_tree_view(path,space = 0):

    for name in os.listdir(path):
        path = os.path.join(path, name)
        print '  '*(space) + ' +'+name
        if os.path.isdir(path):
            new_space = space + 1
            print_tree_view(path, new_space)


def main(argv):
    if len(argv)>1:
        os.chdir(argv[1])
        path = argv[1]
    else:
        path = os.getcwd()
    print_tree_view(path)


if __name__=="__main__":
    main(sys.argv)


