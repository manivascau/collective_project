import os
import sys


def get_content(cale,type_of_content):
    """ Get content based on the type searched"""
    list_of_content=[]
    for name in os.listdir(cale):
        path = os.path.join(cale, name)
        if type_of_content=='dirs':
            if os.path.isdir(path):
                list_of_content.append(name)
        elif type_of_content=='files':
            if os.path.isfile(path):
                list_of_content.append(name)
    return sorted(list_of_content)


def get_number_of_files(path):
    """Obtain number of files"""
    return len([f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))])


def obtain_file_size(size):
    """ Obtain file size"""
    ending = { 1: 'KB', 2: 'MB', 3: 'GB'}
    if size < 1024:
        return '{:7} {}'.format(str(size), 'B')
    i = 1
    while size>1024 and i<3:
        size = float(size)/1024
        i += 1
    return '{:6.3f} {:>3}'.format(size,ending[i-1])


def colored_text(text, color):

    return '\x1b[' + str(color) + 'm'+text+'\x1b[' + str(0) + 'm'


def print_content(path):
    """ Print obtained content """
    dirs = get_content(path,'dirs')
    files = get_content(path,'files')
    if dirs:
        for x in dirs:
            print colored_text('{:20} {:>3} {}'.format(x+'/   ',str(get_number_of_files(x)), ' items'), 94)
    if files:
        for x in files:
            #Check if hidden file
            if not x.startswith('.'):
                print colored_text('{:15} {:>5} {:>10} '.format(x.split('.')[0],'.'+ x.split(".")[-1], \
                                                                obtain_file_size(os.path.getsize(x))), 32)
            else:
                print colored_text('{:15} {:>5} {:>10} '.format(x.split('.')[0], '.' + x.split(".")[-1], \
                                                                obtain_file_size(os.path.getsize(x))),30)


def main(argv):
    if len(argv)>1:
        os.chdir(argv[1])
        path_to_execute = argv[1]
    else:
        path_to_execute = os.getcwd()
    print_content(path_to_execute)


if __name__=="__main__":
    main(sys.argv)


