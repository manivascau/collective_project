# this function should return a tuple
# the first element is a list of integers representing the isbn
# the second element is the check digit
# parse_isbn('ISBN817525766-0') == ([8, 1, 7, 5, 2, 5, 7, 6, 6], 0)

def parse_isbn(isbn):
	isbn_check = int(isbn[-1])
	isbn_main = [int(el) for el in isbn[4:-2]]

	return isbn_main, isbn_check

def is_isbn_valid(isbn):
    checksum = 0
    numbers_to_check = parse_isbn(isbn)[0]
    for i in range(len(numbers_to_check)):
        checksum += numbers_to_check[i] * len(numbers_to_check)+1 - i
    return checksum % 11 == 0

# py.test code below

def test_parse_isbn():
    assert parse_isbn('ISBN817525766-0') == ([8, 1, 7, 5, 2, 5, 7, 6, 6], 0)
    assert not parse_isbn('ISBN817525763-0') == ([8, 1, 7, 5, 2, 5, 7, 6, 6], 0)

def test_is_isbn_valid():
    assert is_isbn_valid('ISBN817525766-0')
    assert not is_isbn_valid('ISBN817525767-0')

print parse_isbn('ISBN817525766-0')
print is_isbn_valid('ISBN817525766-0')



